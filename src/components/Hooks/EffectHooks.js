import React, { useState, useEffect } from 'react';
import styles from './Hooks.module.scss';

const codeString = `
const EffectHooks = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(document.title);
    document.title = \`You clicked \${count} times\`;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me!
      </button>
    </div>
  );
};`;

const EffectHooks = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(document.title);
    document.title = `You clicked ${count} times`;
  });

  return (
    <div className={styles.executableSegment}>
      <p>
        {`You clicked ${count} times`}
      </p>
      <button type="button" onClick={() => setCount(count + 1)}>
        Click me!
      </button>
      <hr />
      <code className={styles.codeSegment}>{codeString}</code>
    </div>
  );
};

export default EffectHooks;
