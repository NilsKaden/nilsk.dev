import React, { useEffect } from 'react';
import styles from './Hooks.module.scss';

const codeString = `
const SubscriptionEffectHooks = () => {
  useEffect(() => {
    console.log('Mounting....');

    return () => console.log('Unmounting....');
  });

  return 'Loading...';
};
`;

const SubscriptionEffectHooks = () => {
  useEffect(() => {
    console.log('Mounting....');

    return () => console.log('Unmounting....');
  });

  return (
    <div className={styles.executableSegment}>
      <p>Loading...</p>
      <hr />
      <code className={styles.codeSegment}>{codeString}</code>
    </div>

  );
};

export default SubscriptionEffectHooks;
