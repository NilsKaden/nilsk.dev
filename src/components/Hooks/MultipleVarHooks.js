import React, { useState } from 'react';
import styles from './Hooks.module.scss';

const codeString = `
const MultipleVarHooks = () => {
  // Declare multiple state variables!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState('banana');
  const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);

  return (
    <div>
      <p onClick={() => setAge(age + 1)}>{age}</p>
      <p onClick={() => setFruit(fruit + 'anotherFruit')}>{fruit}</p>
    </div>
  );
};
`;

const MultipleVarHooks = () => {
  // Declare multiple state variables!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState('banana');
  const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);

  return (
    <div className={styles.executableSegment}>
      <span>Click these: </span>
      <p onClick={() => setAge(age + 1)}>{age}</p>
      <p onClick={() => setFruit(fruit + ' anotherFruit')}>{fruit}</p>
      <div onClick={() => setTodos([...todos, { text: ' teach hooks' }])}>
        {todos.map((e, i) => <span key={`${e.text  } + ${i}`}>{e.text}</span>)}
      </div>
      <hr />
      <code className={styles.codeSegment}>{codeString}</code>
    </div>
  );
};

export default MultipleVarHooks;
