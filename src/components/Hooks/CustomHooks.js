import React from 'react';
import styles from './Hooks.module.scss';

const codeString1 = `
  function FriendStatus(props) {
    const [isOnline, setIsOnline] = useState(null);

    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    useEffect(() => {
      ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
      return () => {
        ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
      };
    });

    if (isOnline === null) {
      return 'Loading...';
    }
    return isOnline ? 'Online' : 'Offline';
  }
`;

const codeString2 = `
  import React, { useState, useEffect } from 'react';

  function FriendListItem(props) {
    const [isOnline, setIsOnline] = useState(null);

    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    useEffect(() => {
      ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
      return () => {
        ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
      };
    });

    return (
      <li style={{ color: isOnline ? 'green' : 'black' }}>
        {props.friend.name}
      </li>
    );
  }
`;

const hookString = `
  import React, { useState, useEffect } from 'react';

  function useFriendStatus(friendID) {
    const [isOnline, setIsOnline] = useState(null);

    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    useEffect(() => {
      ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
      return () => {
        ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
      };
    });

    return isOnline;
  }
`;

const codeString1WithHook = `
  function FriendStatus(props) {
    const isOnline = useFriendStatus(props.friend.id);

    if (isOnline === null) {
      return 'Loading...';
    }
    return isOnline ? 'Online' : 'Offline';
  }
`;

const CustomHooks = () => {
  return (
    <div className={styles.executableSegment}>
      <p>status quo: two components rely on an external API, which triggers a callback </p>
      <code className={styles.codeSegment}>{codeString1}</code>
      <hr />

      <p>Example 2</p>
      <code className={styles.codeSegment}>{codeString2}</code>
      <hr />

      <p>Can be combined into a single hook</p>
      <code className={styles.codeSegment}>{hookString}</code>
      <hr />

      <p>now we can just use the hook to simplify our code</p>
      <code className={styles.codeSegment}>{codeString1WithHook}</code>
    </div>
  );
};

export default CustomHooks;
