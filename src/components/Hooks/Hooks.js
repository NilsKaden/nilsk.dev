import React, { useState } from 'react';
import styles from './Hooks.module.scss';
import OneVarHook from './OneVarHook';
import MultipleVarHooks from './MultipleVarHooks';
import EffectHooks from './EffectHooks';
import SubscriptionEffectHooks from './SubscriptionEffectHooks';
import CustomHooks from './CustomHooks';

const Hooks = () => {
  const [toggle, setToggle] = useState(true);

  return (
    <div>
      <h1>React Hooks</h1>
      <p>Hooks let you use lifecycle methods and state inside functional components.</p>
      <p>useState() is similar to setState(), but no merging is performed.</p>
      <p>hook-methods use named exports and need to be imported like this:</p>

      <code className={styles.codeSegment} dangerouslySetInnerHTML = {{ __html: `import React, { useState } from 'react';` }} />{/* eslint-disable-line */}

      <p>State does not need to be an object anymore. Any type is now allowed</p>
      <h3>Basic Hook Usage</h3>
      <OneVarHook />

      <p>When declaring multiple values from state, react assumes we are gonna call setState() in the same order during every render</p>
      <h3>Using Multiple Variables From State</h3>
      <MultipleVarHooks />

      <p>We can also write our own hooks to hook into</p>
      <p>useEffect serves the same purpose as componentDidMount, componentDidUpdate and componentWillUnmount, but unifies them in a single API</p>
      <p>useEffect gets called after each render, including the inital one</p>
      <h3>Effect Hooks</h3>
      <EffectHooks />

      <h3>Subscribe and Unsubscribe with Effect Hooks</h3>
      <p>Multiple useEffect()s can be used in a single component</p>
      <p>The following components uses the useEffect hook to trigger functions after mounting/unmounting. Have a look at the console or the line below.</p>
      <button type="button" onClick={() => setToggle(!toggle)}>Mount/Unmount</button>
      {toggle ? <SubscriptionEffectHooks /> : null}

      <h3>What (not) to do</h3>
      <p>Make sure to only call hooks at the top level, never inside of loops, nested functions, etc.</p>
      <p>Only call hooks from React components (...duh)</p>

      <h3>Our own hooks</h3>
      <p>share statefull logic between components</p>
      <p>If a function name starts with 'use', it is meant to be reused</p>
      <p>Each call to a hook has an isolated state, so can use the same custom Hook multiple times, even inside a single component</p>
      <p>Custom hooks are more a convention than a feature</p>
      <p>Usefull for: form handling, animation, declarative subscribtions, timers...</p>
      <CustomHooks />

      <h3>Other build in Hooks</h3>
      <p>useContext() for injecting a context directly where you need it.</p>
      <p>const [todos, dispatch] = useReducer(todoReducer)</p>

      <h4>Advantages</h4>
      <ul>
        <li>Combining Mounting and Unmounting inside a single API clears up the whole process. This becomes especiall apparent when binding/unbinding event listeners etc. during the components lifecycle</li>
        <li>Using JS Classes is unintuitive, due to the dynamic binding of 'this'. Hooks circumvent the problem and thus dont need to take part in the madness</li>
        <li>Esotheric bindings, such as: this.myFunction = this.myFunction.bind(this) are no longer necessary in hooks!</li>
      </ul>
    </div>
  );
};

export default Hooks;
