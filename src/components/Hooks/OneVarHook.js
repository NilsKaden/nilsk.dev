import React, { useState } from 'react';
import styles from './Hooks.module.scss';

const componentAsString = `
const Example = () => {
  const [count, setCount] = useState(0);
  //    [state, setterFunction] = useState(initalValue);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
};
`;

const Example = () => {
  const [count, setCount] = useState(0);
  //    [state, setterFunction] = useState(initalValue);

  return (
    <div className={styles.executableSegment}>
      <p>
        {`You clicked ${count} times`}
      </p>
      <button type="button" onClick={() => setCount(count + 1)}>
        Click me!
      </button>

      <hr />

      <code className={styles.codeSegment}>
        {componentAsString}
      </code>
    </div>
  );
};

export default Example;
