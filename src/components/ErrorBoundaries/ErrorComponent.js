import React, { Component } from 'react';

export default class ErrorComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      possiblyNullArray: ['one', 'two'],
    };
  }

  static getDerivedStateFromError(error) {
    console.error(error);
    return ({ hasError: false, possiblyNullArray: ['four', 'three'] });
  }

  render() {
    const { hasError } = this.state;
    let { possiblyNullArray } = this.state;

    // fails half the time, leading to an error below
    if (Math.random() * 100 < 50) {
      possiblyNullArray = null;
      console.log(possiblyNullArray);
    }

    possiblyNullArray.forEach(e => console.log(e));

    return (
      hasError
        ? <h1>hide yo kids, hide yo wife.</h1>
        : <h1>ain't nobody got time for errors</h1>
    );
  }
}
