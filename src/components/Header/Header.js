import React from 'react';
import styles from './Header.module.scss';

const Header = () => (
  <div className={styles.hue}>
    <span>Hello World!</span>
  </div>
);

export default Header;
